const express = require('express');
const nunjucks = require('nunjucks');

const { solve } = require('./equation');
const { print } = require('./print');

const app = express();

app.use(express.static('public'));

nunjucks.configure('views', {
    autoescape: true,
    express: app
});

app.get('/', (req, res) => {
    res.render('index.html');
});

app.get('/solve', (req, res) => {
    const { a, b, c } = req.query;
    res.render('otchet.html', { a, b, c, ...solve(a, b, c) });
});

app.get('/save', (req, res) => {
    const { a, b, c } = req.query;
    res.setHeader('Content-disposition', `attachment; filename=result_${a}_${b}_${c}.txt`);
    res.send(print(solve(a, b, c)));
});

const port = process.env.PORT || 3000;

app.listen(port, () => console.log(`Server is listening at ${port}`));
