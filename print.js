exports.print = ({ discriminant, x1, x2 }) => {
    let result = `D=${discriminant}, `;
    if (discriminant < 0) {
        result += 'Нет действительных корней!';
    } else if (discriminant === 0) {
        result += `x=${x1}`;
    } else {
        result += `x1=${x1}, x2=${x2}`;
    }
    return result;
};
