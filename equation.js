const solve = (a, b, c) => {
    let x1 = null;
    let x2 = null;

    const discriminant = getDiscriminant(a, b, c);

    if (discriminant >= 0) {
        x1 = (-b + Math.sqrt(discriminant)) / 2 * a;
        x2 = (-b - Math.sqrt(discriminant)) / 2 * a;
    }

    return { discriminant, x1, x2 };
};

const getDiscriminant = (a, b ,c) => {
    return Math.pow(b, 2) - 4 * a * c;
};

exports.solve = solve;